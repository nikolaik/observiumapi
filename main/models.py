# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#     * Rearrange models' order
#     * Make sure each model has one field with primary_key=True
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin.py sqlcustom [appname]'
# into your database.
#
# TODO : uniqueness constraints, autofields

from django.db import models

class Alerts(models.Model):
    connection_name = 'observium'

    importance = models.IntegerField()
    device_id = models.IntegerField()
    message = models.TextField()
    time_logged = models.DateTimeField()
    alerted = models.IntegerField()
    class Meta:
        db_table = u'alerts'

class Applications(models.Model):
    connection_name = 'observium'

    app_id = models.AutoField(primary_key=True)
    device_id = models.IntegerField()
    app_type = models.CharField(max_length=192)
    class Meta:
        db_table = u'applications'

class Authlog(models.Model):
    connection_name = 'observium'

    datetime = models.DateTimeField()
    user = models.TextField()
    address = models.TextField()
    result = models.TextField()
    class Meta:
        db_table = u'authlog'

class Bgppeers(models.Model):
    connection_name = 'observium'

    bgppeer_id = models.AutoField(primary_key=True, db_column='bgpPeer_id') # Field name made lowercase.
    device_id = models.IntegerField(db_index=True)
    astext = models.CharField(max_length=192)
    bgppeeridentifier = models.TextField(db_column='bgpPeerIdentifier') # Field name made lowercase.
    bgppeerremoteas = models.IntegerField(db_column='bgpPeerRemoteAs') # Field name made lowercase.
    bgppeerstate = models.TextField(db_column='bgpPeerState') # Field name made lowercase.
    bgppeeradminstatus = models.TextField(db_column='bgpPeerAdminStatus') # Field name made lowercase.
    bgplocaladdr = models.TextField(db_column='bgpLocalAddr') # Field name made lowercase.
    bgppeerremoteaddr = models.TextField(db_column='bgpPeerRemoteAddr') # Field name made lowercase.
    bgppeerinupdates = models.IntegerField(db_column='bgpPeerInUpdates') # Field name made lowercase.
    bgppeeroutupdates = models.IntegerField(db_column='bgpPeerOutUpdates') # Field name made lowercase.
    bgppeerintotalmessages = models.IntegerField(db_column='bgpPeerInTotalMessages') # Field name made lowercase.
    bgppeerouttotalmessages = models.IntegerField(db_column='bgpPeerOutTotalMessages') # Field name made lowercase.
    bgppeerfsmestablishedtime = models.IntegerField(db_column='bgpPeerFsmEstablishedTime') # Field name made lowercase.
    bgppeerinupdateelapsedtime = models.IntegerField(db_column='bgpPeerInUpdateElapsedTime') # Field name made lowercase.
    class Meta:
        db_table = u'bgpPeers'

class BgppeersCbgp(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(db_index=True)
    bgppeeridentifier = models.CharField(max_length=192, db_column='bgpPeerIdentifier') # Field name made lowercase.
    afi = models.CharField(unique=True, max_length=48)
    safi = models.CharField(unique=True, max_length=48)
    acceptedprefixes = models.IntegerField(db_column='AcceptedPrefixes') # Field name made lowercase.
    deniedprefixes = models.IntegerField(db_column='DeniedPrefixes') # Field name made lowercase.
    prefixadminlimit = models.IntegerField(db_column='PrefixAdminLimit') # Field name made lowercase.
    prefixthreshold = models.IntegerField(db_column='PrefixThreshold') # Field name made lowercase.
    prefixclearthreshold = models.IntegerField(db_column='PrefixClearThreshold') # Field name made lowercase.
    advertisedprefixes = models.IntegerField(db_column='AdvertisedPrefixes') # Field name made lowercase.
    suppressedprefixes = models.IntegerField(db_column='SuppressedPrefixes') # Field name made lowercase.
    withdrawnprefixes = models.IntegerField(db_column='WithdrawnPrefixes') # Field name made lowercase.
    class Meta:
        db_table = u'bgpPeers_cbgp'

class BillData(models.Model):
    connection_name = 'observium'

    bill_id = models.IntegerField()
    timestamp = models.DateTimeField()
    period = models.IntegerField()
    delta = models.BigIntegerField()
    in_delta = models.BigIntegerField()
    out_delta = models.BigIntegerField()
    class Meta:
        db_table = u'bill_data'

class BillHistory(models.Model):
    connection_name = 'observium'

    bill_hist_id = models.AutoField(primary_key=True)
    bill_id = models.IntegerField()
    updated = models.DateTimeField()
    bill_datefrom = models.DateTimeField(unique=True)
    bill_dateto = models.DateTimeField(unique=True)
    bill_type = models.TextField()
    bill_allowed = models.BigIntegerField()
    bill_used = models.BigIntegerField()
    bill_overuse = models.BigIntegerField()
    bill_percent = models.DecimalField(max_digits=12, decimal_places=2)
    rate_95th_in = models.BigIntegerField()
    rate_95th_out = models.BigIntegerField()
    rate_95th = models.BigIntegerField()
    dir_95th = models.CharField(max_length=9)
    rate_average = models.BigIntegerField()
    rate_average_in = models.BigIntegerField()
    rate_average_out = models.BigIntegerField()
    traf_in = models.BigIntegerField()
    traf_out = models.BigIntegerField()
    traf_total = models.BigIntegerField()
    pdf = models.TextField(blank=True)
    class Meta:
        db_table = u'bill_history'

class BillPerms(models.Model):
    connection_name = 'observium'

    user_id = models.IntegerField()
    bill_id = models.IntegerField()
    class Meta:
        db_table = u'bill_perms'

class BillPorts(models.Model):
    connection_name = 'observium'

    bill_id = models.IntegerField()
    port_id = models.IntegerField()
    bill_port_autoadded = models.IntegerField()
    class Meta:
        db_table = u'bill_ports'

class Bills(models.Model):
    connection_name = 'observium'

    bill_id = models.IntegerField(unique=True)
    bill_name = models.TextField()
    bill_type = models.TextField()
    bill_cdr = models.IntegerField(null=True, blank=True)
    bill_day = models.IntegerField()
    bill_gb = models.IntegerField(null=True, blank=True)
    rate_95th_in = models.IntegerField()
    rate_95th_out = models.IntegerField()
    rate_95th = models.IntegerField()
    dir_95th = models.CharField(max_length=9)
    total_data = models.IntegerField()
    total_data_in = models.IntegerField()
    total_data_out = models.IntegerField()
    rate_average_in = models.IntegerField()
    rate_average_out = models.IntegerField()
    rate_average = models.IntegerField()
    bill_last_calc = models.DateTimeField()
    bill_custid = models.CharField(max_length=192)
    bill_ref = models.CharField(max_length=192)
    bill_notes = models.CharField(max_length=768)
    bill_autoadded = models.IntegerField()
    class Meta:
        db_table = u'bills'

class CefSwitching(models.Model):
    connection_name = 'observium'

    cef_switching_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField(unique=True)
    entphysicalindex = models.IntegerField(unique=True, db_column='entPhysicalIndex') # Field name made lowercase.
    afi = models.CharField(unique=True, max_length=12)
    cef_index = models.IntegerField(unique=True)
    cef_path = models.CharField(max_length=48)
    drop = models.IntegerField()
    punt = models.IntegerField()
    punt2host = models.IntegerField()
    drop_prev = models.IntegerField()
    punt_prev = models.IntegerField()
    punt2host_prev = models.IntegerField()
    updated = models.IntegerField()
    updated_prev = models.IntegerField()
    class Meta:
        db_table = u'cef_switching'

class Customers(models.Model):
    connection_name = 'observium'

    customer_id = models.IntegerField(primary_key=True)
    username = models.CharField(unique=True, max_length=192)
    password = models.CharField(max_length=96)
    string = models.CharField(max_length=192)
    level = models.IntegerField()
    class Meta:
        db_table = u'customers'

class Dbschema(models.Model):
    connection_name = 'observium'

    revision = models.IntegerField(primary_key=True)
    class Meta:
        db_table = u'dbSchema'

class DeviceGraphs(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField()
    graph = models.CharField(max_length=96)
    class Meta:
        db_table = u'device_graphs'

class Devices(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(primary_key=True)
    hostname = models.CharField(max_length=384)
    sysname = models.CharField(max_length=384, db_column='sysName', blank=True) # Field name made lowercase.
    community = models.CharField(max_length=765, blank=True)
    snmpver = models.CharField(max_length=12)
    port = models.IntegerField()
    transport = models.CharField(max_length=48)
    timeout = models.IntegerField(null=True, blank=True)
    retries = models.IntegerField(null=True, blank=True)
    bgplocalas = models.CharField(max_length=48, db_column='bgpLocalAs', blank=True) # Field name made lowercase.
    sysobjectid = models.CharField(max_length=192, db_column='sysObjectID', blank=True) # Field name made lowercase.
    sysdescr = models.TextField(db_column='sysDescr', blank=True) # Field name made lowercase.
    syscontact = models.TextField(db_column='sysContact', blank=True) # Field name made lowercase.
    version = models.TextField(blank=True)
    hardware = models.TextField(blank=True)
    features = models.TextField(blank=True)
    location = models.TextField(blank=True)
    os = models.CharField(max_length=96, blank=True)
    status = models.IntegerField()
    ignore = models.IntegerField()
    disabled = models.IntegerField()
    uptime = models.BigIntegerField(null=True, blank=True)
    agent_uptime = models.IntegerField()
    last_polled = models.DateTimeField(null=True, blank=True)
    last_polled_timetaken = models.FloatField(null=True, blank=True)
    last_discovered_timetaken = models.FloatField(null=True, blank=True)
    last_discovered = models.DateTimeField(null=True, blank=True)
    purpose = models.CharField(max_length=192, blank=True)
    type = models.CharField(max_length=60)
    serial = models.TextField(blank=True)
    class Meta:
        db_table = u'devices'

class DevicesAttribs(models.Model):
    connection_name = 'observium'

    attrib_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField()
    attrib_type = models.CharField(max_length=96)
    attrib_value = models.TextField()
    updated = models.DateTimeField()
    class Meta:
        db_table = u'devices_attribs'

class DevicesPerms(models.Model):
    connection_name = 'observium'

    user_id = models.IntegerField()
    device_id = models.IntegerField()
    access_level = models.IntegerField()
    class Meta:
        db_table = u'devices_perms'

class Entphysical(models.Model):
    connection_name = 'observium'

    entphysical_id = models.IntegerField(primary_key=True, db_column='entPhysical_id') # Field name made lowercase.
    device_id = models.IntegerField()
    entphysicalindex = models.IntegerField(db_column='entPhysicalIndex') # Field name made lowercase.
    entphysicaldescr = models.TextField(db_column='entPhysicalDescr') # Field name made lowercase.
    entphysicalclass = models.TextField(db_column='entPhysicalClass') # Field name made lowercase.
    entphysicalname = models.TextField(db_column='entPhysicalName') # Field name made lowercase.
    entphysicalhardwarerev = models.CharField(max_length=192, db_column='entPhysicalHardwareRev', blank=True) # Field name made lowercase.
    entphysicalfirmwarerev = models.CharField(max_length=192, db_column='entPhysicalFirmwareRev', blank=True) # Field name made lowercase.
    entphysicalsoftwarerev = models.CharField(max_length=192, db_column='entPhysicalSoftwareRev', blank=True) # Field name made lowercase.
    entphysicalalias = models.CharField(max_length=96, db_column='entPhysicalAlias', blank=True) # Field name made lowercase.
    entphysicalassetid = models.CharField(max_length=96, db_column='entPhysicalAssetID', blank=True) # Field name made lowercase.
    entphysicalisfru = models.CharField(max_length=24, db_column='entPhysicalIsFRU', blank=True) # Field name made lowercase.
    entphysicalmodelname = models.TextField(db_column='entPhysicalModelName') # Field name made lowercase.
    entphysicalvendortype = models.TextField(db_column='entPhysicalVendorType', blank=True) # Field name made lowercase.
    entphysicalserialnum = models.TextField(db_column='entPhysicalSerialNum') # Field name made lowercase.
    entphysicalcontainedin = models.IntegerField(db_column='entPhysicalContainedIn') # Field name made lowercase.
    entphysicalparentrelpos = models.IntegerField(db_column='entPhysicalParentRelPos') # Field name made lowercase.
    entphysicalmfgname = models.TextField(db_column='entPhysicalMfgName') # Field name made lowercase.
    ifindex = models.IntegerField(null=True, db_column='ifIndex', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'entPhysical'

class EntphysicalState(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField()
    entphysicalindex = models.CharField(max_length=192, db_column='entPhysicalIndex') # Field name made lowercase.
    subindex = models.CharField(max_length=192, blank=True)
    group = models.CharField(max_length=192)
    key = models.CharField(max_length=192)
    value = models.CharField(max_length=765)
    class Meta:
        db_table = u'entPhysical_state'

class Eventlog(models.Model):
    connection_name = 'observium'

    event_id = models.IntegerField(primary_key=True)
    host = models.IntegerField()
    datetime = models.DateTimeField()
    message = models.TextField(blank=True)
    type = models.CharField(max_length=192, blank=True)
    reference = models.CharField(max_length=192)
    class Meta:
        db_table = u'eventlog'

class GraphTypes(models.Model):
    connection_name = 'observium'

    graph_type = models.CharField(max_length=96)
    graph_subtype = models.CharField(max_length=96)
    graph_section = models.CharField(max_length=96)
    graph_descr = models.CharField(max_length=192)
    graph_order = models.IntegerField()
    class Meta:
        db_table = u'graph_types'

class GraphTypesDead(models.Model):
    connection_name = 'observium'

    graph_type = models.CharField(max_length=96)
    graph_subtype = models.CharField(max_length=96)
    graph_section = models.CharField(max_length=96)
    graph_descr = models.CharField(max_length=192)
    graph_order = models.IntegerField()
    class Meta:
        db_table = u'graph_types_dead'

class Hrdevice(models.Model):
    connection_name = 'observium'

    hrdevice_id = models.IntegerField(primary_key=True, db_column='hrDevice_id') # Field name made lowercase.
    device_id = models.IntegerField()
    hrdeviceindex = models.IntegerField(db_column='hrDeviceIndex') # Field name made lowercase.
    hrdevicedescr = models.TextField(db_column='hrDeviceDescr') # Field name made lowercase.
    hrdevicetype = models.TextField(db_column='hrDeviceType') # Field name made lowercase.
    hrdeviceerrors = models.IntegerField(db_column='hrDeviceErrors') # Field name made lowercase.
    hrdevicestatus = models.TextField(db_column='hrDeviceStatus') # Field name made lowercase.
    hrprocessorload = models.IntegerField(null=True, db_column='hrProcessorLoad', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'hrDevice'

class IpsecTunnels(models.Model):
    connection_name = 'observium'

    tunnel_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField(unique=True)
    peer_port = models.IntegerField()
    peer_addr = models.CharField(unique=True, max_length=192)
    local_addr = models.CharField(max_length=192)
    local_port = models.IntegerField()
    tunnel_name = models.CharField(max_length=288)
    tunnel_status = models.CharField(max_length=33)
    class Meta:
        db_table = u'ipsec_tunnels'

class Ipv4Addresses(models.Model):
    connection_name = 'observium'

    ipv4_address_id = models.IntegerField(primary_key=True)
    ipv4_address = models.CharField(max_length=96)
    ipv4_prefixlen = models.IntegerField()
    ipv4_network_id = models.CharField(max_length=96)
    interface_id = models.IntegerField()
    class Meta:
        db_table = u'ipv4_addresses'

class Ipv4Mac(models.Model):
    connection_name = 'observium'

    interface_id = models.IntegerField()
    mac_address = models.CharField(max_length=96)
    ipv4_address = models.CharField(max_length=96)
    class Meta:
        db_table = u'ipv4_mac'

class Ipv4Networks(models.Model):
    connection_name = 'observium'

    ipv4_network_id = models.IntegerField(primary_key=True)
    ipv4_network = models.CharField(max_length=192)
    class Meta:
        db_table = u'ipv4_networks'

class Ipv6Addresses(models.Model):
    connection_name = 'observium'

    ipv6_address_id = models.IntegerField(primary_key=True)
    ipv6_address = models.CharField(max_length=384)
    ipv6_compressed = models.CharField(max_length=384)
    ipv6_prefixlen = models.IntegerField()
    ipv6_origin = models.CharField(max_length=48)
    ipv6_network_id = models.CharField(max_length=384)
    interface_id = models.IntegerField()
    class Meta:
        db_table = u'ipv6_addresses'

class Ipv6Networks(models.Model):
    connection_name = 'observium'

    ipv6_network_id = models.IntegerField(primary_key=True)
    ipv6_network = models.CharField(max_length=192)
    class Meta:
        db_table = u'ipv6_networks'

class Juniatmvp(models.Model):
    connection_name = 'observium'

    juniatmvp_id = models.IntegerField(db_column='juniAtmVp_id') # Field name made lowercase.
    interface_id = models.IntegerField()
    vp_id = models.IntegerField()
    vp_descr = models.CharField(max_length=96)
    class Meta:
        db_table = u'juniAtmVp'

class Links(models.Model):
    connection_name = 'observium'

    local_interface_id = models.IntegerField(null=True, blank=True)
    remote_interface_id = models.IntegerField(null=True, blank=True)
    active = models.IntegerField()
    protocol = models.CharField(max_length=33, blank=True)
    remote_hostname = models.CharField(max_length=384)
    remote_port = models.CharField(max_length=384)
    remote_platform = models.CharField(max_length=384)
    remote_version = models.CharField(max_length=768)
    class Meta:
        db_table = u'links'

class MacAccounting(models.Model):
    connection_name = 'observium'

    ma_id = models.IntegerField(primary_key=True)
    interface_id = models.IntegerField()
    mac = models.CharField(max_length=96)
    in_oid = models.CharField(max_length=384)
    out_oid = models.CharField(max_length=384)
    bps_out = models.IntegerField()
    bps_in = models.IntegerField()
    cipmachcswitchedbytes_input = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedBytes_input', blank=True) # Field name made lowercase.
    cipmachcswitchedbytes_input_prev = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedBytes_input_prev', blank=True) # Field name made lowercase.
    cipmachcswitchedbytes_input_delta = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedBytes_input_delta', blank=True) # Field name made lowercase.
    cipmachcswitchedbytes_input_rate = models.IntegerField(null=True, db_column='cipMacHCSwitchedBytes_input_rate', blank=True) # Field name made lowercase.
    cipmachcswitchedbytes_output = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedBytes_output', blank=True) # Field name made lowercase.
    cipmachcswitchedbytes_output_prev = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedBytes_output_prev', blank=True) # Field name made lowercase.
    cipmachcswitchedbytes_output_delta = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedBytes_output_delta', blank=True) # Field name made lowercase.
    cipmachcswitchedbytes_output_rate = models.IntegerField(null=True, db_column='cipMacHCSwitchedBytes_output_rate', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_input = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedPkts_input', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_input_prev = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedPkts_input_prev', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_input_delta = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedPkts_input_delta', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_input_rate = models.IntegerField(null=True, db_column='cipMacHCSwitchedPkts_input_rate', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_output = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedPkts_output', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_output_prev = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedPkts_output_prev', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_output_delta = models.BigIntegerField(null=True, db_column='cipMacHCSwitchedPkts_output_delta', blank=True) # Field name made lowercase.
    cipmachcswitchedpkts_output_rate = models.IntegerField(null=True, db_column='cipMacHCSwitchedPkts_output_rate', blank=True) # Field name made lowercase.
    poll_time = models.IntegerField(null=True, blank=True)
    poll_prev = models.IntegerField(null=True, blank=True)
    poll_period = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'mac_accounting'

class Mempools(models.Model):
    connection_name = 'observium'

    mempool_id = models.IntegerField(primary_key=True)
    mempool_index = models.CharField(max_length=48)
    entphysicalindex = models.IntegerField(null=True, db_column='entPhysicalIndex', blank=True) # Field name made lowercase.
    hrdeviceindex = models.IntegerField(null=True, db_column='hrDeviceIndex', blank=True) # Field name made lowercase.
    mempool_type = models.CharField(max_length=96)
    mempool_precision = models.IntegerField()
    mempool_descr = models.CharField(max_length=192)
    device_id = models.IntegerField()
    mempool_perc = models.IntegerField()
    mempool_used = models.BigIntegerField()
    mempool_free = models.BigIntegerField()
    mempool_total = models.BigIntegerField()
    mempool_largestfree = models.BigIntegerField(null=True, blank=True)
    mempool_lowestfree = models.BigIntegerField(null=True, blank=True)
    class Meta:
        db_table = u'mempools'

class OspfAreas(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(unique=True)
    ospfareaid = models.CharField(unique=True, max_length=96, db_column='ospfAreaId') # Field name made lowercase.
    ospfauthtype = models.CharField(max_length=192, db_column='ospfAuthType') # Field name made lowercase.
    ospfimportasextern = models.CharField(max_length=384, db_column='ospfImportAsExtern') # Field name made lowercase.
    ospfspfruns = models.IntegerField(db_column='ospfSpfRuns') # Field name made lowercase.
    ospfareabdrrtrcount = models.IntegerField(db_column='ospfAreaBdrRtrCount') # Field name made lowercase.
    ospfasbdrrtrcount = models.IntegerField(db_column='ospfAsBdrRtrCount') # Field name made lowercase.
    ospfarealsacount = models.IntegerField(db_column='ospfAreaLsaCount') # Field name made lowercase.
    ospfarealsacksumsum = models.IntegerField(db_column='ospfAreaLsaCksumSum') # Field name made lowercase.
    ospfareasummary = models.CharField(max_length=192, db_column='ospfAreaSummary') # Field name made lowercase.
    ospfareastatus = models.CharField(max_length=192, db_column='ospfAreaStatus') # Field name made lowercase.
    class Meta:
        db_table = u'ospf_areas'

class OspfInstances(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(unique=True)
    ospf_instance_id = models.IntegerField(unique=True)
    ospfrouterid = models.CharField(max_length=96, db_column='ospfRouterId') # Field name made lowercase.
    ospfadminstat = models.CharField(max_length=96, db_column='ospfAdminStat') # Field name made lowercase.
    ospfversionnumber = models.CharField(max_length=96, db_column='ospfVersionNumber') # Field name made lowercase.
    ospfareabdrrtrstatus = models.CharField(max_length=96, db_column='ospfAreaBdrRtrStatus') # Field name made lowercase.
    ospfasbdrrtrstatus = models.CharField(max_length=96, db_column='ospfASBdrRtrStatus') # Field name made lowercase.
    ospfexternlsacount = models.IntegerField(db_column='ospfExternLsaCount') # Field name made lowercase.
    ospfexternlsacksumsum = models.IntegerField(db_column='ospfExternLsaCksumSum') # Field name made lowercase.
    ospftossupport = models.CharField(max_length=96, db_column='ospfTOSSupport') # Field name made lowercase.
    ospforiginatenewlsas = models.IntegerField(db_column='ospfOriginateNewLsas') # Field name made lowercase.
    ospfrxnewlsas = models.IntegerField(db_column='ospfRxNewLsas') # Field name made lowercase.
    ospfextlsdblimit = models.IntegerField(null=True, db_column='ospfExtLsdbLimit', blank=True) # Field name made lowercase.
    ospfmulticastextensions = models.IntegerField(null=True, db_column='ospfMulticastExtensions', blank=True) # Field name made lowercase.
    ospfexitoverflowinterval = models.IntegerField(null=True, db_column='ospfExitOverflowInterval', blank=True) # Field name made lowercase.
    ospfdemandextensions = models.CharField(max_length=96, db_column='ospfDemandExtensions', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'ospf_instances'

class OspfNbrs(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(unique=True)
    interface_id = models.IntegerField()
    ospf_nbr_id = models.CharField(unique=True, max_length=96)
    ospfnbripaddr = models.CharField(max_length=96, db_column='ospfNbrIpAddr') # Field name made lowercase.
    ospfnbraddresslessindex = models.IntegerField(db_column='ospfNbrAddressLessIndex') # Field name made lowercase.
    ospfnbrrtrid = models.CharField(max_length=96, db_column='ospfNbrRtrId') # Field name made lowercase.
    ospfnbroptions = models.IntegerField(db_column='ospfNbrOptions') # Field name made lowercase.
    ospfnbrpriority = models.IntegerField(db_column='ospfNbrPriority') # Field name made lowercase.
    ospfnbrstate = models.CharField(max_length=96, db_column='ospfNbrState') # Field name made lowercase.
    ospfnbrevents = models.IntegerField(db_column='ospfNbrEvents') # Field name made lowercase.
    ospfnbrlsretransqlen = models.IntegerField(db_column='ospfNbrLsRetransQLen') # Field name made lowercase.
    ospfnbmanbrstatus = models.CharField(max_length=96, db_column='ospfNbmaNbrStatus') # Field name made lowercase.
    ospfnbmanbrpermanence = models.CharField(max_length=96, db_column='ospfNbmaNbrPermanence') # Field name made lowercase.
    ospfnbrhellosuppressed = models.CharField(max_length=96, db_column='ospfNbrHelloSuppressed') # Field name made lowercase.
    class Meta:
        db_table = u'ospf_nbrs'

class OspfPorts(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(unique=True)
    interface_id = models.IntegerField()
    ospf_port_id = models.CharField(unique=True, max_length=96)
    ospfifipaddress = models.CharField(max_length=96, db_column='ospfIfIpAddress') # Field name made lowercase.
    ospfaddresslessif = models.IntegerField(db_column='ospfAddressLessIf') # Field name made lowercase.
    ospfifareaid = models.CharField(max_length=96, db_column='ospfIfAreaId') # Field name made lowercase.
    ospfiftype = models.CharField(max_length=96, db_column='ospfIfType', blank=True) # Field name made lowercase.
    ospfifadminstat = models.CharField(max_length=96, db_column='ospfIfAdminStat', blank=True) # Field name made lowercase.
    ospfifrtrpriority = models.IntegerField(null=True, db_column='ospfIfRtrPriority', blank=True) # Field name made lowercase.
    ospfiftransitdelay = models.IntegerField(null=True, db_column='ospfIfTransitDelay', blank=True) # Field name made lowercase.
    ospfifretransinterval = models.IntegerField(null=True, db_column='ospfIfRetransInterval', blank=True) # Field name made lowercase.
    ospfifhellointerval = models.IntegerField(null=True, db_column='ospfIfHelloInterval', blank=True) # Field name made lowercase.
    ospfifrtrdeadinterval = models.IntegerField(null=True, db_column='ospfIfRtrDeadInterval', blank=True) # Field name made lowercase.
    ospfifpollinterval = models.IntegerField(null=True, db_column='ospfIfPollInterval', blank=True) # Field name made lowercase.
    ospfifstate = models.CharField(max_length=96, db_column='ospfIfState', blank=True) # Field name made lowercase.
    ospfifdesignatedrouter = models.CharField(max_length=96, db_column='ospfIfDesignatedRouter', blank=True) # Field name made lowercase.
    ospfifbackupdesignatedrouter = models.CharField(max_length=96, db_column='ospfIfBackupDesignatedRouter', blank=True) # Field name made lowercase.
    ospfifevents = models.IntegerField(null=True, db_column='ospfIfEvents', blank=True) # Field name made lowercase.
    ospfifauthkey = models.CharField(max_length=384, db_column='ospfIfAuthKey', blank=True) # Field name made lowercase.
    ospfifstatus = models.CharField(max_length=96, db_column='ospfIfStatus', blank=True) # Field name made lowercase.
    ospfifmulticastforwarding = models.CharField(max_length=96, db_column='ospfIfMulticastForwarding', blank=True) # Field name made lowercase.
    ospfifdemand = models.CharField(max_length=96, db_column='ospfIfDemand', blank=True) # Field name made lowercase.
    ospfifauthtype = models.CharField(max_length=96, db_column='ospfIfAuthType', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'ospf_ports'

class PerfTimes(models.Model):
    connection_name = 'observium'

    type = models.CharField(max_length=24)
    doing = models.CharField(max_length=192)
    start = models.IntegerField()
    duration = models.FloatField()
    devices = models.IntegerField()
    class Meta:
        db_table = u'perf_times'

class PortInMeasurements(models.Model):
    connection_name = 'observium'

    port_id = models.IntegerField()
    timestamp = models.DateTimeField()
    counter = models.BigIntegerField()
    delta = models.BigIntegerField()
    class Meta:
        db_table = u'port_in_measurements'

class PortOutMeasurements(models.Model):
    connection_name = 'observium'

    port_id = models.IntegerField()
    timestamp = models.DateTimeField()
    counter = models.BigIntegerField()
    delta = models.BigIntegerField()
    class Meta:
        db_table = u'port_out_measurements'

class Ports(models.Model):
    connection_name = 'observium'

    interface_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField(unique=True)
    port_descr_type = models.CharField(max_length=765, blank=True)
    port_descr_descr = models.CharField(max_length=765, blank=True)
    port_descr_circuit = models.CharField(max_length=765, blank=True)
    port_descr_speed = models.CharField(max_length=96, blank=True)
    port_descr_notes = models.CharField(max_length=765, blank=True)
    ifdescr = models.CharField(max_length=765, db_column='ifDescr', blank=True) # Field name made lowercase.
    ifname = models.CharField(max_length=192, db_column='ifName', blank=True) # Field name made lowercase.
    portname = models.CharField(max_length=384, db_column='portName', blank=True) # Field name made lowercase.
    ifindex = models.IntegerField(unique=True, null=True, db_column='ifIndex', blank=True) # Field name made lowercase.
    ifspeed = models.BigIntegerField(null=True, db_column='ifSpeed', blank=True) # Field name made lowercase.
    ifconnectorpresent = models.CharField(max_length=36, db_column='ifConnectorPresent', blank=True) # Field name made lowercase.
    ifpromiscuousmode = models.CharField(max_length=36, db_column='ifPromiscuousMode', blank=True) # Field name made lowercase.
    ifhighspeed = models.IntegerField(null=True, db_column='ifHighSpeed', blank=True) # Field name made lowercase.
    ifoperstatus = models.CharField(max_length=48, db_column='ifOperStatus', blank=True) # Field name made lowercase.
    ifadminstatus = models.CharField(max_length=48, db_column='ifAdminStatus', blank=True) # Field name made lowercase.
    ifduplex = models.CharField(max_length=36, db_column='ifDuplex', blank=True) # Field name made lowercase.
    ifmtu = models.IntegerField(null=True, db_column='ifMtu', blank=True) # Field name made lowercase.
    iftype = models.TextField(db_column='ifType', blank=True) # Field name made lowercase.
    ifalias = models.TextField(db_column='ifAlias', blank=True) # Field name made lowercase.
    ifphysaddress = models.TextField(db_column='ifPhysAddress', blank=True) # Field name made lowercase.
    ifhardtype = models.CharField(max_length=192, db_column='ifHardType', blank=True) # Field name made lowercase.
    iflastchange = models.DateTimeField(db_column='ifLastChange') # Field name made lowercase.
    ifvlan = models.CharField(max_length=24, db_column='ifVlan') # Field name made lowercase.
    iftrunk = models.CharField(max_length=24, db_column='ifTrunk', blank=True) # Field name made lowercase.
    ifvrf = models.IntegerField(db_column='ifVrf') # Field name made lowercase.
    counter_in = models.IntegerField(null=True, blank=True)
    counter_out = models.IntegerField(null=True, blank=True)
    ignore = models.IntegerField()
    disabled = models.IntegerField()
    detailed = models.IntegerField()
    deleted = models.IntegerField()
    pagpoperationmode = models.CharField(max_length=96, db_column='pagpOperationMode', blank=True) # Field name made lowercase.
    pagpportstate = models.CharField(max_length=48, db_column='pagpPortState', blank=True) # Field name made lowercase.
    pagppartnerdeviceid = models.CharField(max_length=144, db_column='pagpPartnerDeviceId', blank=True) # Field name made lowercase.
    pagppartnerlearnmethod = models.CharField(max_length=48, db_column='pagpPartnerLearnMethod', blank=True) # Field name made lowercase.
    pagppartnerifindex = models.IntegerField(null=True, db_column='pagpPartnerIfIndex', blank=True) # Field name made lowercase.
    pagppartnergroupifindex = models.IntegerField(null=True, db_column='pagpPartnerGroupIfIndex', blank=True) # Field name made lowercase.
    pagppartnerdevicename = models.CharField(max_length=384, db_column='pagpPartnerDeviceName', blank=True) # Field name made lowercase.
    pagpethcoperationmode = models.CharField(max_length=48, db_column='pagpEthcOperationMode', blank=True) # Field name made lowercase.
    pagpdeviceid = models.CharField(max_length=144, db_column='pagpDeviceId', blank=True) # Field name made lowercase.
    pagpgroupifindex = models.IntegerField(null=True, db_column='pagpGroupIfIndex', blank=True) # Field name made lowercase.
    ifinucastpkts = models.BigIntegerField(null=True, db_column='ifInUcastPkts', blank=True) # Field name made lowercase.
    ifinucastpkts_prev = models.BigIntegerField(null=True, db_column='ifInUcastPkts_prev', blank=True) # Field name made lowercase.
    ifinucastpkts_delta = models.BigIntegerField(null=True, db_column='ifInUcastPkts_delta', blank=True) # Field name made lowercase.
    ifinucastpkts_rate = models.IntegerField(null=True, db_column='ifInUcastPkts_rate', blank=True) # Field name made lowercase.
    ifoutucastpkts = models.BigIntegerField(null=True, db_column='ifOutUcastPkts', blank=True) # Field name made lowercase.
    ifoutucastpkts_prev = models.BigIntegerField(null=True, db_column='ifOutUcastPkts_prev', blank=True) # Field name made lowercase.
    ifoutucastpkts_delta = models.BigIntegerField(null=True, db_column='ifOutUcastPkts_delta', blank=True) # Field name made lowercase.
    ifoutucastpkts_rate = models.IntegerField(null=True, db_column='ifOutUcastPkts_rate', blank=True) # Field name made lowercase.
    ifinerrors = models.BigIntegerField(null=True, db_column='ifInErrors', blank=True) # Field name made lowercase.
    ifinerrors_prev = models.BigIntegerField(null=True, db_column='ifInErrors_prev', blank=True) # Field name made lowercase.
    ifinerrors_delta = models.BigIntegerField(null=True, db_column='ifInErrors_delta', blank=True) # Field name made lowercase.
    ifinerrors_rate = models.IntegerField(null=True, db_column='ifInErrors_rate', blank=True) # Field name made lowercase.
    ifouterrors = models.BigIntegerField(null=True, db_column='ifOutErrors', blank=True) # Field name made lowercase.
    ifouterrors_prev = models.BigIntegerField(null=True, db_column='ifOutErrors_prev', blank=True) # Field name made lowercase.
    ifouterrors_delta = models.BigIntegerField(null=True, db_column='ifOutErrors_delta', blank=True) # Field name made lowercase.
    ifouterrors_rate = models.IntegerField(null=True, db_column='ifOutErrors_rate', blank=True) # Field name made lowercase.
    ifinoctets = models.BigIntegerField(null=True, db_column='ifInOctets', blank=True) # Field name made lowercase.
    ifinoctets_prev = models.BigIntegerField(null=True, db_column='ifInOctets_prev', blank=True) # Field name made lowercase.
    ifinoctets_delta = models.BigIntegerField(null=True, db_column='ifInOctets_delta', blank=True) # Field name made lowercase.
    ifinoctets_rate = models.IntegerField(null=True, db_column='ifInOctets_rate', blank=True) # Field name made lowercase.
    ifoutoctets = models.BigIntegerField(null=True, db_column='ifOutOctets', blank=True) # Field name made lowercase.
    ifoutoctets_prev = models.BigIntegerField(null=True, db_column='ifOutOctets_prev', blank=True) # Field name made lowercase.
    ifoutoctets_delta = models.BigIntegerField(null=True, db_column='ifOutOctets_delta', blank=True) # Field name made lowercase.
    ifoutoctets_rate = models.IntegerField(null=True, db_column='ifOutOctets_rate', blank=True) # Field name made lowercase.
    poll_time = models.IntegerField(null=True, blank=True)
    poll_prev = models.IntegerField(null=True, blank=True)
    poll_period = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'ports'

class PortsAdsl(models.Model):
    connection_name = 'observium'

    interface_id = models.IntegerField(unique=True)
    port_adsl_updated = models.DateTimeField()
    adsllinecoding = models.CharField(max_length=24, db_column='adslLineCoding') # Field name made lowercase.
    adsllinetype = models.CharField(max_length=48, db_column='adslLineType') # Field name made lowercase.
    adslatucinvvendorid = models.CharField(max_length=24, db_column='adslAtucInvVendorID') # Field name made lowercase.
    adslatucinvversionnumber = models.CharField(max_length=24, db_column='adslAtucInvVersionNumber') # Field name made lowercase.
    adslatuccurrsnrmgn = models.DecimalField(decimal_places=1, max_digits=7, db_column='adslAtucCurrSnrMgn') # Field name made lowercase.
    adslatuccurratn = models.DecimalField(decimal_places=1, max_digits=7, db_column='adslAtucCurrAtn') # Field name made lowercase.
    adslatuccurroutputpwr = models.DecimalField(decimal_places=1, max_digits=7, db_column='adslAtucCurrOutputPwr') # Field name made lowercase.
    adslatuccurrattainablerate = models.IntegerField(db_column='adslAtucCurrAttainableRate') # Field name made lowercase.
    adslatucchancurrtxrate = models.IntegerField(db_column='adslAtucChanCurrTxRate') # Field name made lowercase.
    adslaturinvserialnumber = models.CharField(max_length=24, db_column='adslAturInvSerialNumber') # Field name made lowercase.
    adslaturinvvendorid = models.CharField(max_length=24, db_column='adslAturInvVendorID') # Field name made lowercase.
    adslaturinvversionnumber = models.CharField(max_length=24, db_column='adslAturInvVersionNumber') # Field name made lowercase.
    adslaturchancurrtxrate = models.IntegerField(db_column='adslAturChanCurrTxRate') # Field name made lowercase.
    adslaturcurrsnrmgn = models.DecimalField(decimal_places=1, max_digits=7, db_column='adslAturCurrSnrMgn') # Field name made lowercase.
    adslaturcurratn = models.DecimalField(decimal_places=1, max_digits=7, db_column='adslAturCurrAtn') # Field name made lowercase.
    adslaturcurroutputpwr = models.DecimalField(decimal_places=1, max_digits=7, db_column='adslAturCurrOutputPwr') # Field name made lowercase.
    adslaturcurrattainablerate = models.IntegerField(db_column='adslAturCurrAttainableRate') # Field name made lowercase.
    class Meta:
        db_table = u'ports_adsl'

class PortsPerms(models.Model):
    connection_name = 'observium'

    user_id = models.IntegerField()
    interface_id = models.IntegerField()
    access_level = models.IntegerField()
    class Meta:
        db_table = u'ports_perms'

class PortsStack(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(unique=True)
    interface_id_high = models.IntegerField(unique=True)
    interface_id_low = models.IntegerField(unique=True)
    ifstackstatus = models.CharField(max_length=96, db_column='ifStackStatus') # Field name made lowercase.
    class Meta:
        db_table = u'ports_stack'

class PortsVlans(models.Model):
    connection_name = 'observium'

    port_vlan_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField(unique=True)
    interface_id = models.IntegerField(unique=True)
    vlan = models.IntegerField(unique=True)
    baseport = models.IntegerField()
    priority = models.BigIntegerField()
    state = models.CharField(max_length=48)
    cost = models.IntegerField()
    class Meta:
        db_table = u'ports_vlans'

class Processors(models.Model):
    connection_name = 'observium'

    processor_id = models.IntegerField(primary_key=True)
    entphysicalindex = models.IntegerField(db_column='entPhysicalIndex') # Field name made lowercase.
    hrdeviceindex = models.IntegerField(null=True, db_column='hrDeviceIndex', blank=True) # Field name made lowercase.
    device_id = models.IntegerField()
    processor_oid = models.CharField(max_length=384)
    processor_index = models.CharField(max_length=96)
    processor_type = models.CharField(max_length=48)
    processor_usage = models.IntegerField()
    processor_descr = models.CharField(max_length=192)
    processor_precision = models.IntegerField()
    class Meta:
        db_table = u'processors'

class Pseudowires(models.Model):
    connection_name = 'observium'

    pseudowire_id = models.IntegerField(primary_key=True)
    interface_id = models.IntegerField()
    peer_device_id = models.IntegerField()
    peer_ldp_id = models.IntegerField()
    cpwvcid = models.IntegerField(db_column='cpwVcID') # Field name made lowercase.
    cpwoid = models.IntegerField(db_column='cpwOid') # Field name made lowercase.
    class Meta:
        db_table = u'pseudowires'

class Sensors(models.Model):
    connection_name = 'observium'

    sensor_id = models.IntegerField(primary_key=True)
    sensor_class = models.CharField(max_length=192)
    device_id = models.IntegerField()
    poller_type = models.CharField(max_length=48)
    sensor_oid = models.CharField(max_length=765)
    sensor_index = models.CharField(max_length=30)
    sensor_type = models.CharField(max_length=765)
    sensor_descr = models.CharField(max_length=765, blank=True)
    sensor_divisor = models.IntegerField()
    sensor_multiplier = models.IntegerField()
    sensor_current = models.FloatField(null=True, blank=True)
    sensor_limit = models.FloatField(null=True, blank=True)
    sensor_limit_warn = models.FloatField(null=True, blank=True)
    sensor_limit_low = models.FloatField(null=True, blank=True)
    sensor_limit_low_warn = models.FloatField(null=True, blank=True)
    entphysicalindex = models.CharField(max_length=48, db_column='entPhysicalIndex', blank=True) # Field name made lowercase.
    entphysicalindex_measured = models.CharField(max_length=48, db_column='entPhysicalIndex_measured', blank=True) # Field name made lowercase.
    class Meta:
        db_table = u'sensors'

class Services(models.Model):
    connection_name = 'observium'

    service_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField()
    service_ip = models.TextField()
    service_type = models.CharField(max_length=48)
    service_desc = models.TextField()
    service_param = models.TextField()
    service_ignore = models.IntegerField()
    service_status = models.IntegerField()
    service_checked = models.IntegerField()
    service_changed = models.IntegerField()
    service_message = models.TextField()
    service_disabled = models.IntegerField()
    class Meta:
        db_table = u'services'

class Storage(models.Model):
    connection_name = 'observium'

    storage_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField()
    storage_mib = models.CharField(unique=True, max_length=48)
    storage_index = models.IntegerField(unique=True)
    storage_type = models.CharField(max_length=96, blank=True)
    storage_descr = models.TextField()
    storage_size = models.BigIntegerField()
    storage_units = models.IntegerField()
    storage_used = models.BigIntegerField()
    storage_free = models.BigIntegerField()
    storage_perc = models.TextField()
    storage_perc_warn = models.IntegerField(null=True, blank=True)
    class Meta:
        db_table = u'storage'

class Syslog(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField(null=True, blank=True)
    facility = models.CharField(max_length=30, blank=True)
    priority = models.CharField(max_length=30, blank=True)
    level = models.CharField(max_length=30, blank=True)
    tag = models.CharField(max_length=30, blank=True)
    timestamp = models.DateTimeField()
    program = models.CharField(max_length=96, blank=True)
    msg = models.TextField(blank=True)
    seq = models.BigIntegerField(primary_key=True)
    class Meta:
        db_table = u'syslog'

class Toner(models.Model):
    connection_name = 'observium'

    toner_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField()
    toner_index = models.IntegerField()
    toner_type = models.CharField(max_length=192)
    toner_oid = models.CharField(max_length=192)
    toner_descr = models.CharField(max_length=96)
    toner_capacity = models.IntegerField()
    toner_current = models.IntegerField()
    class Meta:
        db_table = u'toner'

class UcdDiskio(models.Model):
    connection_name = 'observium'

    diskio_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField()
    diskio_index = models.IntegerField()
    diskio_descr = models.CharField(max_length=96)
    class Meta:
        db_table = u'ucd_diskio'

class Users(models.Model):
    connection_name = 'observium'

    user_id = models.IntegerField(primary_key=True)
    username = models.CharField(unique=True, max_length=90)
    password = models.CharField(max_length=102, blank=True)
    realname = models.CharField(max_length=192)
    email = models.CharField(max_length=192)
    descr = models.CharField(max_length=90)
    level = models.IntegerField()
    can_modify_passwd = models.IntegerField()
    class Meta:
        db_table = u'users'

class UsersPrefs(models.Model):
    connection_name = 'observium'

    user_id = models.IntegerField(unique=True)
    pref = models.CharField(max_length=96)
    value = models.CharField(max_length=384)
    class Meta:
        db_table = u'users_prefs'

class Vlans(models.Model):
    connection_name = 'observium'

    vlan_id = models.IntegerField(primary_key=True)
    device_id = models.IntegerField(null=True, blank=True)
    vlan_vlan = models.IntegerField(null=True, blank=True)
    vlan_domain = models.TextField(blank=True)
    vlan_descr = models.TextField(blank=True)
    class Meta:
        db_table = u'vlans'

class Vminfo(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField()
    vm_type = models.CharField(max_length=48)
    vmwvmvmid = models.IntegerField(db_column='vmwVmVMID') # Field name made lowercase.
    vmwvmdisplayname = models.CharField(max_length=384, db_column='vmwVmDisplayName') # Field name made lowercase.
    vmwvmguestos = models.CharField(max_length=384, db_column='vmwVmGuestOS') # Field name made lowercase.
    vmwvmmemsize = models.IntegerField(db_column='vmwVmMemSize') # Field name made lowercase.
    vmwvmcpus = models.IntegerField(db_column='vmwVmCpus') # Field name made lowercase.
    vmwvmstate = models.CharField(max_length=384, db_column='vmwVmState') # Field name made lowercase.
    class Meta:
        db_table = u'vminfo'

class VmwareVminfo(models.Model):
    connection_name = 'observium'

    device_id = models.IntegerField()
    vmwvmvmid = models.IntegerField(db_column='vmwVmVMID') # Field name made lowercase.
    vmwvmdisplayname = models.CharField(max_length=384, db_column='vmwVmDisplayName') # Field name made lowercase.
    vmwvmguestos = models.CharField(max_length=384, db_column='vmwVmGuestOS') # Field name made lowercase.
    vmwvmmemsize = models.IntegerField(db_column='vmwVmMemSize') # Field name made lowercase.
    vmwvmcpus = models.IntegerField(db_column='vmwVmCpus') # Field name made lowercase.
    vmwvmstate = models.CharField(max_length=384, db_column='vmwVmState') # Field name made lowercase.
    class Meta:
        db_table = u'vmware_vminfo'

class Vrfs(models.Model):
    connection_name = 'observium'

    vrf_id = models.IntegerField(primary_key=True)
    vrf_oid = models.CharField(max_length=768)
    vrf_name = models.CharField(max_length=384, blank=True)
    mplsvpnvrfroutedistinguisher = models.CharField(max_length=384, db_column='mplsVpnVrfRouteDistinguisher', blank=True) # Field name made lowercase.
    mplsvpnvrfdescription = models.TextField(db_column='mplsVpnVrfDescription') # Field name made lowercase.
    device_id = models.IntegerField()
    class Meta:
        db_table = u'vrfs'

